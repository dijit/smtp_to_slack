#!/usr/bin/python

import smtplib

sender = 'msv@hunter-prod'
receivers = ['test@msv-liveops.slack']

message = """From: From Person <from@fromdomain.com>
To: To Person <to@todomain.com>
Subject: SMTP e-mail test

This is a test e-mail message.
With multiple lines.
"""

message = """Return-path: <*******@massive.se>
Original-recipient: rfc822;******@me.com
Received: from UNKNOWN by st13p27ic-hpcx16283301 (mailgateway 2007B140)
	with SMTP id f2c7b708-85e0-4c39-85e4-b561ac2d21cf 
	for <*****@me.com>; Wed, 6 May 2020 11:58:17 GMT
Received: from 17.58.36.84 by null (mailnotify null); Wed, 6 May 2020 11:58:17 GMT
X-Apple-MoveToFolder: INBOX 
X-Apple-Action: MOVE_TO_FOLDER/INBOX
X-Apple-UUID: f2c7b708-85e0-4c39-85e4-b561ac2d21cf
Received: from smtp1.linuxfoundation.org (smtp1.linuxfoundation.org [140.211.169.13])
	by ms11p00im-qufv17093901.me.com (Postfix) with ESMTPS id 2139B4C044D
	for <*****@me.com>; Wed,  6 May 2020 11:58:15 +0000 (UTC)
Authentication-Results: magent0590.usmsc04.pie.apple.com; dmarc=pass header.from=massive.se
x-dmarc-info: pass=pass; dmarc-policy=none; s=r0; d=r1
x-dmarc-policy: v=DMARC1; p=none; pct=100; rua=mailto:dmarcreport@ubisoft.com
Authentication-Results: magent1026.usmsc04.pie.apple.com;
	dkim=pass (1024-bit key) header.d=massive.se header.i=@massive.se header.b=qEv6eSR4
Authentication-Results: magent0441.usmsc04.pie.apple.com; spf=softfail (magent0441.usmsc04.pie.apple.com: domain of transitioning jan.harasym@massive.se does not designate 140.211.169.13 as permitted sender) smtp.mailfrom=jan.harasym@massive.se
Received-SPF: softfail (magent0441.usmsc04.pie.apple.com: domain of transitioning jan.harasym@massive.se does not designate 140.211.169.13 as permitted sender) receiver=magent0441.usmsc04.pie.apple.com; client-ip=140.211.169.13; helo=smtp1.linuxfoundation.org; envelope-from=jan.harasym@massive.se
X-Greylist: whitelisted by SQLgrey-1.7.6
Received: from NAM04-CO1-obe.outbound.protection.outlook.com (mail-eopbgr690086.outbound.protection.outlook.com [40.107.69.86])
	by smtp1.linuxfoundation.org (Postfix) with ESMTPS id 5382325A
	for <*****@linux.com>; Wed,  6 May 2020 11:58:13 +0000 (UTC)
ARC-Seal: i=1; a=rsa-sha256; s=arcselector9901; d=microsoft.com; cv=none;
 b=Qdc8Q/XUm2dCsD1A8DfKo+DNiUvmRMI3qO3i6Ng+meHs1s3GGhZqPx7Uyn0pjQ1RpEFyMzfK+Sz6WpdeSGMa8kfHpE7uAx0aWFavuWyaKG/HwUv20ntO6MVwRyIGQ41fB2FE/7RlehGsFdUp6m5wCrOyvEA0e+akpVZKv1n+GO3N5+Rvtj2cIFfyoCou7Xz4cTYGcBGzUakNSeShz4m9e5fY6Gt99KyfvEtvHG4eSlBP1KS0iRbSYjKvVw/8mtC8garpOXP1u6MWkWf0/OAi4f3uk1+Uv1Iju7bu1stltMeBvdFSM+sVSOadi2b31rL4dx+NIQb3tZW5XASz7+LiSw==
ARC-Message-Signature: i=1; a=rsa-sha256; c=relaxed/relaxed; d=microsoft.com;
 s=arcselector9901;
 h=From:Date:Subject:Message-ID:Content-Type:MIME-Version:X-MS-Exchange-SenderADCheck;
 bh=Q651zLQak1Jnd6EnP0VgJU4fnyi5rHyfsUMzFwXlAXA=;
 b=nroqhzaxWXCvFnjwXZIVh68lcbdxbElLfm1DT1+IDhqe81jpr3OmUIpdB5aXdIKZZ3PcEfRloi+vYPIgOKf6O5t6cc3Bj8MvIANfmZEDJWJi+qUz7ARh4r9T4cFBIja83iLpgiJXmtSBenKXsdlvhUySYrw4RYTmc7BedhV3KFBQj04Yiq4HODdWTsJ0oS96ul4ti8oFve0MzYs+TM6pocZ9bQd3r27rYW17bKAstYGwYUGv10ETBqpbv4TD4ZVUDS1oHmbrxIDnEAX5NkJcqN3mTU6+OlqAlbnKbHVC6E8J89+WJxIlquHO+ec8GYUPBwB4iUNmaNuFPLC/qNZIWw==
ARC-Authentication-Results: i=1; mx.microsoft.com 1; spf=pass
 smtp.mailfrom=massive.se; dmarc=pass action=none header.from=massive.se;
 dkim=pass header.d=massive.se; arc=none
DKIM-Signature: v=1; a=rsa-sha256; c=relaxed/relaxed; d=massive.se;
 s=selector2;
 h=From:Date:Subject:Message-ID:Content-Type:MIME-Version:X-MS-Exchange-SenderADCheck;
 bh=Q651zLQak1Jnd6EnP0VgJU4fnyi5rHyfsUMzFwXlAXA=;
 b=qEv6eSR4LK9qdAX8KQ2h4su7On0pFfsQZstAkSi+ui1egc1fFmjriu0JUJ54ejGnYyUW2M+80m4qIBLwcDBpYGaLsy/k/RPWuDJrg2PXSnOMBvkQAbQ5/K5QUmwTKikf3k2GA56z2kVHXX63nlPBqtHhBSFkNVwS9ZJyXfQXmSo=
Received: from MWHPR17MB1168.namprd17.prod.outlook.com (2603:10b6:300:98::18)
 by MWHPR17MB0991.namprd17.prod.outlook.com (2603:10b6:300:9e::8) with
 Microsoft SMTP Server (version=TLS1_2,
 cipher=TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384) id 15.20.2958.19; Wed, 6 May
 2020 11:58:12 +0000
Received: from MWHPR17MB1168.namprd17.prod.outlook.com
 ([fe80::2488:8b4a:42df:daf5]) by MWHPR17MB1168.namprd17.prod.outlook.com
 ([fe80::2488:8b4a:42df:daf5%4]) with mapi id 15.20.2958.030; Wed, 6 May 2020
 11:58:12 +0000
From: ME <********@massive.se>
To: SLACK <livecrashes@msv-liveops.slack>
Subject: [ps4 msr-tctd-s4cs01.ubisoft.onbe] minidumps hourly list
Thread-Topic: [ps4 msr-tctd-s4cs01.ubisoft.onbe] minidumps hourly list
Thread-Index: AQHWI5a0P3ULBFukgUOJ9yeWy/KGu6ia6L/QgAALPFk=
Date: Wed, 6 May 2020 11:58:12 +0000
Message-ID:
 <MWHPR17MB1168A137555F9B8970B339CF82A40@MWHPR17MB1168.namprd17.prod.outlook.com>
References:
 <20200506110839.8A5456150F5@msr-onl-mail-out03.ubisoft.com>,<CH2PR17MB360513FC5B115937197445A9FFA40@CH2PR17MB3605.namprd17.prod.outlook.com>
In-Reply-To:
 <CH2PR17MB360513FC5B115937197445A9FFA40@CH2PR17MB3605.namprd17.prod.outlook.com>
Accept-Language: en-GB, en-US
Content-Language: en-GB
X-MS-Has-Attach:
X-MS-TNEF-Correlator:
authentication-results: linux.com; dkim=none (message not signed)
 header.d=none;linux.com; dmarc=none action=none header.from=massive.se;
x-originating-ip: [81.230.246.251]
x-ms-publictraffictype: Email
x-ms-office365-filtering-correlation-id: c86ed881-da7a-439b-89b2-08d7f1b4c09e
x-ms-traffictypediagnostic: MWHPR17MB0991:
x-microsoft-antispam-prvs:
 <MWHPR17MB0991441CF8AFF003C6F583D282A40@MWHPR17MB0991.namprd17.prod.outlook.com>
x-ms-oob-tlc-oobclassifiers: OLM:3631;
x-forefront-prvs: 03950F25EC
x-ms-exchange-senderadcheck: 1
x-microsoft-antispam: BCL:0;
x-microsoft-antispam-message-info:
 VzqhggCqbNgTS8WYp9xlzBAqQl+DZK76rOy5cLcJXqAVsAoVYRk46CMFMlBEjeDhOpfK6117s+JzFkmCP3Ml8jhqAA3eonQcNrJo3M7h7e6YV78K831G2NUNiK8TfLsoZvGfgiGQqumBF1wmGycn65fTIjc/VfpVbCvn9zs00nMnA6NZt7zy6AR3mfHN0pGghXlNOfmsySIzgwul/szldbF7fafOwNXoGOKQDtfeXMNgXAb45euR+Ewz7r7cHdQWzam8/3PKvj4LDbh50iIl9pj3QHc5YrcfGyUh0dhIGCIwJCQLPMKIbUSa/HBWs1FDq7gUSfLkOTtpWfVhWWmmsk0fafvghPZqWSuIqxnN1z7PKNBcCANFii43GMxJPRKd7yv9jwgTXDCbSsfWcaSkwMdhdfyrGogMxWdX+X+qQYGAQeL59fXq1BVWcp9W3olNLt5depFb64bLcEDzsZB5zMQeYbgxZRXWf7KnpkVDP97AvqV6Xap1QKRQfizL52LPj4l6RDB/ysCQsMekCmYwGS4zD66eweX54CMe2K2wVQsl46X9+P2y5xKTwkB175TCA2HSdtKGeIqfFojUpHoCZq5UkzgLBC/53Tm9SqeT2isrShnbpyS3n815Mu8oVl1ibMrtG03+fAB456UZr1ZVjZ25dQ18tCaeaLH9jicT2Eo=
x-forefront-antispam-report:
 CIP:255.255.255.255;CTRY:;LANG:en;SCL:1;SRV:;IPV:NLI;SFV:NSPM;H:MWHPR17MB1168.namprd17.prod.outlook.com;PTR:;CAT:NONE;SFTY:;SFS:(6019001)(366004)(346002)(396003)(39860400002)(376002)(136003)(279900001)(33430700001)(186003)(83080400001)(52536014)(26005)(5660300002)(6916009)(7066003)(91956017)(19627405001)(7696005)(9686003)(33440700001)(55016002)(316002)(2906002)(33656002)(6506007)(76116006)(53546011)(8676002)(8936002)(86362001)(66946007)(66556008)(478600001)(64756008)(44832011)(66476007)(66446008)(71200400001)(19623455009)(460985005);DIR:OUT;SFP:1101;
x-ms-exchange-antispam-messagedata:
 pVhnE98fFajNb82+MX6l3gwnloz03tSd7TrxtvY8mRpf7qAs9ShbwhJhNMLs2fXBosAD62ugOkrnv7fs30OIXsCOGAUjXm+AqZA4GrT3pbWV6sKsAu8XalaSyhCIGGt1CTpXRSwP77QZhc36Ozz8A2Vq4+S8MIcRE7ZltMwRlEHWR1s/t9waWCYvR9BpO96vlc9l1nwhCTRYfmClWlsAdXz1s0flk4/aqfQ3A8ZTM0MpqpC0jeFZow8r9Vi0PFwiqRaAHVJzDttwUwtQ4k+6wTj+0m29bI1Xh1sKnkmlMyUIsmBrEgmS9jfwmjqsrdEhq4YjhF8i32v1LIc+Cq/w0aICnNeS5ldysO03l/6LWgWWDtO+6ky/HAPn+ksPpK8e1iss1kc55rgioP+xuvIbsd4w97AN0hZHRuG2/6gV2U7xWPnDJgaLq5ATZnk5kiyyGHbb33W82MYqakopBHQuaqFBukXvzbgDmNWUTt6z6prEQ9GB6g9estDdLxGcwDk7aD5m7QMj2gMrk/vJZR+NeCHyHPNCfY1gO0XLMu9/eAAgOxX6qmeAGrb0HJOAk7HN5J11KK+oaSAy1b9rn+SuYZ54RBDuysDJNzYn3JLM3PbJziRf8MpYMWdXu16ExmCtvQ/l/O08FDroZqlzXz1BFe98cgKnFuAA5P0ggkUGRLyQG4LW69vBX4nD7vc/+11i2LSdfuy8jJz0QikcJPsgccx3/crZcUvI03glhbrYdyDyD+vfKThuk+lRb8BGHn5EIG1EhaleJupmu/mzg0rEgbd5+p0B1UNPYGLn7Svv9Vo=
x-ms-exchange-transport-forked: True
Content-Type: multipart/alternative;
MIME-Version: 1.0
X-OriginatorOrg: massive.se
X-MS-Exchange-CrossTenant-Network-Message-Id: c86ed881-da7a-439b-89b2-08d7f1b4c09e
X-MS-Exchange-CrossTenant-originalarrivaltime: 06 May 2020 11:58:12.2587
 (UTC)
X-MS-Exchange-CrossTenant-fromentityheader: Hosted
X-MS-Exchange-CrossTenant-id: e01bd386-fa51-4210-a2a4-29e5ab6f7ab1
X-MS-Exchange-CrossTenant-mailboxtype: HOSTED
X-MS-Exchange-CrossTenant-userprincipalname: c7Mn/uRiGlaHLf31hB9fON2pyHaK8lXinxgWfNVYee88uIwLAUOA8pclHhgM7yfg61TmRs5xKOCwv4tGYHPvOg==
X-MS-Exchange-Transport-CrossTenantHeadersStamped: MWHPR17MB0991
X-Spam-Status: No, score=-1.5 required=5.0 tests=BAYES_00,DKIM_SIGNED,
	DKIM_VALID,DKIM_VALID_AU,HTML_MESSAGE,NUMERIC_HTTP_ADDR,RCVD_IN_DNSWL_LOW,
	WEIRD_PORT autolearn=ham version=3.3.1
X-Spam-Checker-Version: SpamAssassin 3.3.1 (2010-03-16) on
	smtp1.linux-foundation.org
X-MANTSH: 1TEIXR1kbG1oaGkNHB1tfTFwbHRgZGxwaGxEKTEMXGxoEGxsYBBsbGgQeGRAbHho
 fGhEKTFkXGxoaHxEKWUQXbRxZHBlrRnhuUlARCllNF25PRkNcT1gRCl9ZFxgSHhEKX00XZEVET
 xEKWUkXHR9xGwYbHxp3BhscEwYaBhoGEwYacRoQGncGGgYaBhkaBhoGGgYacRoQGncGGhEKWV4
 XbGx5EQpDThdHQR4FQEMFfllYG1xZRBllWGJmW1BmEnptQhkBU18FaREKWFwXGQQaBB4ZBx0cE
 ksdHx9JBRsaBBMEGx0EGBgSEBseGh8aEQpeWRd8HG9YUBEKTVwXGxIRCkxaF2hpTWt7EQpMRhd
 va2trbGtuEQpDWhceGgQbGh0EHBMEEhwRCkJeFxsRCkJGF2F4blxEaxJ9XR1AEQpCXBcbEQpeT
 hcbEQpCSxdjG08Ya1hZU24aexEKQkkXYxtPGGtYWVNuGnsRCkJFF25GaEZ+R3JlXGxuEQpCThd
 jG08Ya1hZU24aexEKQkwXYHtoXR1bWkFobBMRCkJsF2FNTRgaclhnRh1MEQpCQBdobUVJfkt4X
 UtJfBEKQlgXZ21oUlAeTx1dSEYRCkJ4F2d/fGx9c2h5YG9cEQpNXhcHGxEKcGgXbmV4TEBuSFp
 Af0EQBxkaEQpwaBdkeUR+QHN7HmMFRhAHGRoRCnBoF2thTWd5XVpaZnhpEAcZGhEKcGgXb2ZwX
 ltOZmFfGl0QBxkaEQpwaBdsQEBueX5lAQVeYRAHGRoRCnBsF2xSX2h/WE0dH1N8EAcZGhEKbX4
 XBxsRClhNF0sR
X-CLX-Shades: Deliver
X-CLX-UShades: None
X-CLX-Score: 1005
X-CLX-UnSpecialScore: 284
X-CLX-Spam: false
X-Proofpoint-Virus-Version: vendor=fsecure engine=2.50.10434:6.0.138,18.0.676
 definitions=2020-05-06_05:2020-05-05,2020-05-06 signatures=0
X-Proofpoint-Spam-Details: rule=notspam policy=default score=0 spamscore=0 clxscore=1005
 suspectscore=0 malwarescore=0 phishscore=0 adultscore=0 bulkscore=0
 classifier=clx:Deliver adjust=0 reason=mlx scancount=1
 engine=8.0.1-2002250000 definitions=main-2005060095
x-icloud-message-id: 559ee996d70d5f9a85ff8a2edd46e9ae042d749d8b9741c6605572f8b763d777
X-ICL-INFO: GAtbRFYBBVFFSlVHSwAGQhkQEhoNQwsMVQoTShZfWAdVHAJKElFYTB0YFBYLSVsiGBgVBBFGU0wG
 HEYRHA1bAwYKDwEdHkUHVQoPB0UBAlJbS1dGVgEAW1tIVVcQVVoNSAoLAwgBGA4cFxMPHl9DDBEY
 Eh4XXhgNBx5GBwxCCxEYDRZGVlxfDAABABgNXlIDARAJGVZfRAVVHQJKFVFFERwPA1kLVRYPHB1b
 fVgMezU9KTRGT310U0RPXjZJAwFXQEwgTjoID1VFO1VEQXNwWkc4Ukc4fWEqJStXQDVyB1NDQUgZ
 GV1GEBFIUVkIQlkGWxYTAxRfWQlbGgkaRhBEAUgTDhYKUUUbGDkLElZTWQ8=

minidump collector<http://webserver/web/app/minidump_collector/minidump=
_collector.html?host=3Dws://my_hostname:16001/>
total crashes: 269
unique crashes: 1
exe
d:\bin\1.8.6119218\leaderboard.exe
branch
SUBPC
version
6112872
occurrences
269
crash first seen at
2017-12-05 11:17:27
command line
d:/bin/common_config.txt
-multiplex
-live_config_file d:/configs/leaderboard.txt
-process_guid 36B53949-2E1E-43E5-9F7172964F6C69F2
-ns_host ns_hostname
-ns_port 16000
-no_overlord 1
-zabbix_server my_hostname
-zabbix_host zabbix_hostname
-send_data_to_db_time 5M
-weekly_epoch 2017-02-26
-db_port 3306
-statsd_port 8125
-leaderboard_config_file leaderboard.conf
-datasampler_tag my_hostname
-monthly_epoch 2017-02-01
-keyserver_remote_cert d:/certs/keyserver_servercert.pem
-zabbix_send_interval_secs 30
-keyserver_use _ssl 1
-keyserver_local_cert d:/certs/keyserver_clientcert.pem
-db_name db_name
-db_host db_hostname
-use_local_log_server_only 1
-statsd_host statsd_hostname
-db_user rogue
-keyserver_local_key d:/certs/keyserver_clientkey.pem
-max_server_task_threads 4
-db_pass *****
-keyserver_local_key_password *****
assert expression
(!mg::common::CommandLine::GetInstance().IsPresent("leaderboard_config_file=
")): Failed to read config file 'leaderboard.conf'
dump file
crash_dump-767446115-1009.zip<http://10.152.40.20:14602/crash_dump-76744611=
5-1009.zip>
functions
mg::common_servers::AutoDumper::ExceptionFilter
UnhandledExceptionFilter
memset
_C_specific_handler
_chkstk
RtlRaiseException
KiUserExceptionDispatcher
mg::common::AssertS
mg::common::AssertF
mg::leaderboard::ConfigManager::LoadConfig
mg::leaderboard::ConfigManager::ConfigManager
mg::leaderboard::LeaderboardServicesManager::LeaderboardServicesManager
mg::common::Singleton<mg::leaderboard::LeaderboardServicesManager,1>::GetIn=
stanceInternal
mg::common::Singleton<mg::leaderboard::LeaderboardServicesManager,1>::GetIn=
stance
mg::leaderboard::main
main
__tmainCRTStartup
mainCRTStartup
BaseThreadInitThunk
RtlUserThreadStart


--_000_MWHPR17MB1168A137555F9B8970B339CF82A40MWHPR17MB1168namp_
Content-Type: text/html; charset="iso-8859-1"
Content-Transfer-Encoding: quoted-printable

<html>
<head>
<meta http-equiv=3D"Content-Type" content=3D"text/html; charset=3Diso-8859-=
1">
<style type=3D"text/css" style=3D"display:none;"> P {margin-top:0;margin-bo=
ttom:0;} </style>
</head>
<body dir=3D"ltr">
<div style=3D"font-family: Calibri, Arial, Helvetica, sans-serif; font-size=
: 12pt; color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);">
<br>
</div>
<div>
<div>
<div class=3D"WordSection1">
<p class=3D"MsoNormal" style=3D"margin: 0cm 0cm 0.0001pt; font-size: 11pt; =
font-family: Calibri, sans-serif;">
<b>From:</b> ps4@msr-tctd-s4cs01 &lt;ps4@hostname&gt; <br>
<b>Sent:</b> den 6 maj 2020 19:09<br>
<b>To:</b> online-crash &lt;online-crash@some_domain&gt;; game=
-TaskForce-OnlineBackend &lt;Rogue-TaskForce-OnlineBackend@ubisoft.com&gt;<=
br>
<b>Subject:</b> [ps4] minidumps hourly list</p=
>
<p class=3D"MsoNormal" style=3D"margin: 0cm 0cm 0.0001pt; font-size: 11pt; =
font-family: Calibri, sans-serif;">
&nbsp;</p>
<p class=3D"MsoNormal" style=3D"margin: 0cm 0cm 0.0001pt; font-size: 11pt; =
font-family: Calibri, sans-serif;">
<tt style=3D"font-family: &quot;Courier New&quot;;"><span style=3D"font-siz=
e:10.0pt"><a href=3D"http://webserver/web/app/minidump_collector/minidu=
mp_collector.html?host=3Dws://msr-tctd-s4cs01.ubisoft.onbe:16001/" style=3D=
"">minidump collector</a></span></tt><span style=3D"font-size:10.0pt; font-=
family:&quot;Courier New&quot;"><br>
<tt style=3D"font-family: &quot;Courier New&quot;;">total crashes: 269</tt>=
<br>
<tt style=3D"font-family: &quot;Courier New&quot;;">unique crashes: 1</tt><=
/span></p>
<table class=3D"MsoNormalTable" border=3D"1" cellpadding=3D"0" width=3D"100=
%" style=3D"width:100.0%; border:solid black 1.0pt">
<tbody>
<tr>
<td valign=3D"top" style=3D"border:solid black 1.0pt; padding:.75pt .75pt .=
75pt .75pt">
<p class=3D"MsoNormal" align=3D"right" style=3D"margin: 0cm 0cm 0.0001pt; f=
ont-size: 11pt; font-family: Calibri, sans-serif;text-align:right">
<span style=3D"font-size:8.5pt; font-family:&quot;Verdana&quot;,sans-serif"=
>exe</span></p>
</td>
<td valign=3D"top" style=3D"border:solid black 1.0pt; padding:.75pt .75pt .=
75pt .75pt">
<p class=3D"MsoNormal" style=3D"margin: 0cm 0cm 0.0001pt; font-size: 11pt; =
font-family: Calibri, sans-serif;">
<span style=3D"font-size:8.5pt; font-family:&quot;Verdana&quot;,sans-serif"=
>d:\bin\1.8.6119218\leaderboard.exe</span></p>
</td>
</tr>
<tr>
<td valign=3D"top" style=3D"border:solid black 1.0pt; padding:.75pt .75pt .=
75pt .75pt">
<p class=3D"MsoNormal" align=3D"right" style=3D"margin: 0cm 0cm 0.0001pt; f=
ont-size: 11pt; font-family: Calibri, sans-serif;text-align:right">
<span style=3D"font-size:8.5pt; font-family:&quot;Verdana&quot;,sans-serif"=
>branch</span></p>
</td>
<td valign=3D"top" style=3D"border:solid black 1.0pt; padding:.75pt .75pt .=
75pt .75pt">
<p class=3D"MsoNormal" style=3D"margin: 0cm 0cm 0.0001pt; font-size: 11pt; =
font-family: Calibri, sans-serif;">
<span style=3D"font-size:8.5pt; font-family:&quot;Verdana&quot;,sans-serif"=
>SUBPC</span></p>
</td>
</tr>
<tr>
<td valign=3D"top" style=3D"border:solid black 1.0pt; padding:.75pt .75pt .=
75pt .75pt">
<p class=3D"MsoNormal" align=3D"right" style=3D"margin: 0cm 0cm 0.0001pt; f=
ont-size: 11pt; font-family: Calibri, sans-serif;text-align:right">
<span style=3D"font-size:8.5pt; font-family:&quot;Verdana&quot;,sans-serif"=
>version</span></p>
</td>
<td valign=3D"top" style=3D"border:solid black 1.0pt; padding:.75pt .75pt .=
75pt .75pt">
<p class=3D"MsoNormal" style=3D"margin: 0cm 0cm 0.0001pt; font-size: 11pt; =
font-family: Calibri, sans-serif;">
<span style=3D"font-size:8.5pt; font-family:&quot;Verdana&quot;,sans-serif"=
>6112872</span></p>
</td>
</tr>
<tr>
<td valign=3D"top" style=3D"border:solid black 1.0pt; padding:.75pt .75pt .=
75pt .75pt">
<p class=3D"MsoNormal" align=3D"right" style=3D"margin: 0cm 0cm 0.0001pt; f=
ont-size: 11pt; font-family: Calibri, sans-serif;text-align:right">
<span style=3D"font-size:8.5pt; font-family:&quot;Verdana&quot;,sans-serif"=
>occurrences</span></p>
</td>
<td valign=3D"top" style=3D"border:solid black 1.0pt; padding:.75pt .75pt .=
75pt .75pt">
<p class=3D"MsoNormal" style=3D"margin: 0cm 0cm 0.0001pt; font-size: 11pt; =
font-family: Calibri, sans-serif;">
<span style=3D"font-size:8.5pt; font-family:&quot;Verdana&quot;,sans-serif"=
>269</span></p>
</td>
</tr>
<tr>
<td valign=3D"top" style=3D"border:solid black 1.0pt; padding:.75pt .75pt .=
75pt .75pt">
<p class=3D"MsoNormal" align=3D"right" style=3D"margin: 0cm 0cm 0.0001pt; f=
ont-size: 11pt; font-family: Calibri, sans-serif;text-align:right">
<span style=3D"font-size:8.5pt; font-family:&quot;Verdana&quot;,sans-serif"=
>crash first seen at</span></p>
</td>
<td valign=3D"top" style=3D"border:solid black 1.0pt; padding:.75pt .75pt .=
75pt .75pt">
<p class=3D"MsoNormal" style=3D"margin: 0cm 0cm 0.0001pt; font-size: 11pt; =
font-family: Calibri, sans-serif;">
<span style=3D"font-size:8.5pt; font-family:&quot;Verdana&quot;,sans-serif"=
>2017-12-05 11:17:27</span></p>
</td>
</tr>
<tr>
<td valign=3D"top" style=3D"border:solid black 1.0pt; padding:.75pt .75pt .=
75pt .75pt">
<p class=3D"MsoNormal" align=3D"right" style=3D"margin: 0cm 0cm 0.0001pt; f=
ont-size: 11pt; font-family: Calibri, sans-serif;text-align:right">
<span style=3D"font-size:8.5pt; font-family:&quot;Verdana&quot;,sans-serif"=
>command line</span></p>
</td>
<td valign=3D"top" style=3D"border:solid black 1.0pt; padding:.75pt .75pt .=
75pt .75pt">
<p class=3D"MsoNormal" style=3D"margin: 0cm 0cm 0.0001pt; font-size: 11pt; =
font-family: Calibri, sans-serif;">
<span style=3D"font-size:8.5pt; font-family:&quot;Verdana&quot;,sans-serif"=
>d:/rogue/bin/common_config.txt
<br>
-multiplex <br>
-live_config_file d:/configs/leaderboard.txt <br>
-process_guid 36B53949-2E1E-43E5-9F7172964F6C69F2 <br>
-ns_host ns_hostname <br>
-ns_port 16000 <br>
-no_overlord 1 <br>
-zabbix_server zabbix_hostname <br>
-zabbix_host my_hostname <br>
-send_data_to_db_time 5M <br>
-weekly_epoch 2017-02-26 <br>
-db_port 3306 <br>
-statsd_port 8125 <br>
-leaderboard_config_file leaderboard.conf <br>
-datasampler_tag my_hostname <br>
-monthly_epoch 2017-02-01 <br>
-keyserver_remote_cert d:/rogue/certs/keyserver_servercert.pem <br>
-zabbix_send_interval_secs 30 <br>
-keyserver_use _ssl 1 <br>
-keyserver_local_cert d:/rogue/certs/keyserver_clientcert.pem <br>
-db_name db_name <br>
-db_host db_hostname <br>
-use_local_log_server_only 1 <br>
-statsd_host 10.152.15.27 <br>
-db_user db_user <br>
-keyserver_local_key d:/rogue/certs/keyserver_clientkey.pem <br>
-max_server_task_threads 4 <br>
-db_pass [redacted] <br>
-keyserver_local_key_password [redacted] </span></p>
</td>
</tr>
<tr>
<td valign=3D"top" style=3D"border:solid black 1.0pt; padding:.75pt .75pt .=
75pt .75pt">
<p class=3D"MsoNormal" align=3D"right" style=3D"margin: 0cm 0cm 0.0001pt; f=
ont-size: 11pt; font-family: Calibri, sans-serif;text-align:right">
<span style=3D"font-size:8.5pt; font-family:&quot;Verdana&quot;,sans-serif"=
>assert expression</span></p>
</td>
<td valign=3D"top" style=3D"border:solid black 1.0pt; padding:.75pt .75pt .=
75pt .75pt">
<p class=3D"MsoNormal" style=3D"margin: 0cm 0cm 0.0001pt; font-size: 11pt; =
font-family: Calibri, sans-serif;">
<span style=3D"font-size:8.5pt; font-family:&quot;Verdana&quot;,sans-serif"=
>(!mg::common::CommandLine::GetInstance().IsPresent(&quot;leaderboard_confi=
g_file&quot;)): Failed to read config file 'leaderboard.conf'</span></p>
</td>
</tr>
<tr>
<td valign=3D"top" style=3D"border:solid black 1.0pt; padding:.75pt .75pt .=
75pt .75pt">
<p class=3D"MsoNormal" align=3D"right" style=3D"margin: 0cm 0cm 0.0001pt; f=
ont-size: 11pt; font-family: Calibri, sans-serif;text-align:right">
<span style=3D"font-size:8.5pt; font-family:&quot;Verdana&quot;,sans-serif"=
>dump file</span></p>
</td>
<td valign=3D"top" style=3D"border:solid black 1.0pt; padding:.75pt .75pt .=
75pt .75pt">
<p class=3D"MsoNormal" style=3D"margin: 0cm 0cm 0.0001pt; font-size: 11pt; =
font-family: Calibri, sans-serif;">
<span style=3D"font-size:8.5pt; font-family:&quot;Verdana&quot;,sans-serif"=
><a href=3D"http://10.152.40.20:14602/crash_dump-767446115-1009.zip" style=
=3D"">crash_dump-767446115-1009.zip</a></span></p>
</td>
</tr>
<tr>
<td valign=3D"top" style=3D"border:solid black 1.0pt; padding:.75pt .75pt .=
75pt .75pt">
<p class=3D"MsoNormal" align=3D"right" style=3D"margin: 0cm 0cm 0.0001pt; f=
ont-size: 11pt; font-family: Calibri, sans-serif;text-align:right">
<span style=3D"font-size:8.5pt; font-family:&quot;Verdana&quot;,sans-serif"=
>functions</span></p>
</td>
<td valign=3D"top" style=3D"border:solid black 1.0pt; padding:.75pt .75pt .=
75pt .75pt">
<p class=3D"MsoNormal" style=3D"margin: 0cm 0cm 0.0001pt; font-size: 11pt; =
font-family: Calibri, sans-serif;">
<span style=3D"font-size:8.5pt; font-family:&quot;Verdana&quot;,sans-serif"=
>mg::common_servers::AutoDumper::ExceptionFilter<br>
UnhandledExceptionFilter<br>
memset<br>
_C_specific_handler<br>
_chkstk<br>
RtlRaiseException<br>
KiUserExceptionDispatcher<br>
mg::common::AssertS<br>
mg::common::AssertF<br>
mg::leaderboard::ConfigManager::LoadConfig<br>
mg::leaderboard::ConfigManager::ConfigManager<br>
mg::leaderboard::LeaderboardServicesManager::LeaderboardServicesManager<br>
mg::common::Singleton&lt;mg::leaderboard::LeaderboardServicesManager,1&gt;:=
:GetInstanceInternal<br>
mg::common::Singleton&lt;mg::leaderboard::LeaderboardServicesManager,1&gt;:=
:GetInstance<br>
mg::leaderboard::main<br>
main<br>
__tmainCRTStartup<br>
mainCRTStartup<br>
BaseThreadInitThunk<br>
RtlUserThreadStart</span></p>
</td>
</tr>
</tbody>
</table>
<p class=3D"MsoNormal" style=3D"margin: 0cm 0cm 0.0001pt; font-size: 11pt; =
font-family: Calibri, sans-serif;margin-bottom:12.0pt">
&nbsp;</p>
</div>
</div>
</div>
</body>
</html>
"""

try:
   smtpObj = smtplib.SMTP('localhost', 1025)
   smtpObj.sendmail(sender, receivers, message)
   print("Successfully sent email")
except smtplib.SMTPException as e:
   print(f"Error: unable to send email: {e}")