import asyncio
import os
from slack import WebClient
from slack.errors import SlackApiError
import email
from bs4 import BeautifulSoup
import html2text

client = WebClient(
    token=os.environ['SLACK_API_TOKEN'],
    run_async=True
)


def processing_to_markdown(html_text, url, delimiter):
    # Not using "lxml" parser since I get to see a lot of different HTML
    # and the "lxml" parser tend to drop content when parsing very big HTML
    # that has some errors inside
    soup = BeautifulSoup(html_text, "html.parser")

    # Finds all <span class="searched_found">...</span> tags
    for tag in soup.findAll('span', class_="WordSection1"):
        tag.string = delimiter + tag.string + delimiter
        tag.unwrap()  # Removes the tags to only keep the text

    html_text = str(soup)

    ret = html2text.html2text(html_text, url)
    with open("./dump.md", "w") as f:
        f.write(str(ret))
    return "stuff"


async def send_to_slack(channel: str, message: str, loop):

    for line in message.splitlines():
        if line.startswith("Subject: "):
            slack_msg = line.replace("Subject: ", "")
            slack_msg += '\n'

    if not slack_msg:
        print("Failed to find subject in mail, is it malformed?")
        raise KeyError

    mail = email.message_from_string(message)
    slack_msg += processing_to_markdown(mail.get_payload(), None, None)

    future = client.chat_postMessage(
        channel=channel,
        text=slack_msg
    )

    try:
        response = asyncio.ensure_future(future)
    except SlackApiError as e:
        assert e.response["ok"] is False
        assert e.response["error"]  # str like 'invalid_auth', 'channel_not_found'
        print(f"Got an error: {e.response['error']}")
