#from datetime import datetime
import asyncio
from slack_send import send_to_slack
from aiosmtpd.controller import Controller

class CustomHandler:
    async def handle_DATA(self, server, session, envelope):
        peer = session.peer
        mail_from = envelope.mail_from
        rcpt_tos = envelope.rcpt_tos
        data = envelope.content         # type: bytes
        for recipient in rcpt_tos:
            if recipient.endswith('.slack'):
                r = recipient.split("@")
                channel = r[0]
                instance = r[1].split(".")[0]
                print(f"Channel is {channel}, instance is {instance}")
                await send_to_slack(channel, str(data, 'utf-8'), loop=controller.loop)
                del r
                break
        if Exception:
            return '500 Could not process your message'
        return '250 OK'


if __name__ == '__main__':
    handler = CustomHandler()
    controller = Controller(handler, hostname='127.0.0.1', port=1025)
    # Run the event loop in a separate thread.
    controller.start()
    # Wait for the user to press Return.
    input('SMTP server running. Press Return to stop server and exit.')
    controller.stop()

