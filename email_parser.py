from bs4 import BeautifulSoup
import re, html2text
import email

CLEANBODY_RE = re.compile(r'<(/?)(.+?)>', re.M)
htmlformatter = html2text.HTML2Text()
htmlformatter.ignore_links = True
htmlformatter.ignore_images = True
htmlformatter.body_width = 0


def html_to_markdown(html):
    # convert html to markdown.
    # specification:
    # remove all tag except tags listed in the cleanbody._repl
    # when p/div tags are in the table tag, markdown converter (htmlformatter)
    # confuses. so it is removed
    html2 = '<html>'
    record = False
    for line in html.splitlines():
        if '<html>' in line:
            record=True
        elif record == True:
            html2 += line
        else:
            pass
    html = html2

    def remove_non_table_tags_within_table_tag(html):
        def _repl(match):
            tag = match.group(2).split(' ')[0].lower()
            if tag in ('table', 'tr', 'td', 'th'):
                return match.group(0)
            return u''

        soup = BeautifulSoup(html, "lxml")
        for table in soup.find_all('table'):
            # find all table element

            # convert to string
            table_html = str(table)

            # remove tags other than  'table', 'tr', 'td', 'th'
            clean_table = CLEANBODY_RE.sub(_repl, table_html)

            # put back to the original html
            table.replace_with(BeautifulSoup(clean_table, "lxml"))
        return str(soup)

    def non_document_tag_remover(match):
        tag = match.group(2).split(' ')[0].lower()
        if tag in ('br', 'ul', 'li', 'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'div', 'table', 'tr', 'td', 'th'):
            return match.group(0)
        return u''

    # main routine
    if html.lower().find('table') > -1:
        # clean tags within table tag
        html = remove_non_table_tags_within_table_tag(html)

    # strip unrelated tags
    clean_html = CLEANBODY_RE.sub(non_document_tag_remover, html)

    # convert it to markdown
    markdown = htmlformatter.handle(clean_html)

    return markdown


def process_email(html_mail_content: str) -> str:

    for line in html_mail_content.splitlines():
        if line.startswith("Subject: "):
            slack_msg = line.replace("Subject: ", "")
            slack_msg += '\n'

    if not slack_msg:
        print("Failed to find subject in mail, is it malformed?")
        raise KeyError

    mail = email.message_from_string(html_mail_content)
    slack_msg += html_to_markdown(mail.get_payload())
    return f"```{slack_msg}```"


if __name__ == '__main__':
    with open("tests/email.html", 'r') as f:
        html = f.read()
    print(html_to_markdown(html))